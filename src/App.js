import React, { Component } from 'react';
import TodoApp from './containers/todoapp';
import logo from './logo.svg';

class App extends Component {
  render() {
    return (
      <div className="App">
        <TodoApp />
      </div>
    );
  }
}

export default App;
