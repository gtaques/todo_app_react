import React, {Component} from 'react';
import TodoInput from '../components/todo-input/todoinput';
import TodoList from '../components/todo-list/todolist';

class TodoApp extends Component {
    state = {
        todos: []
    }

    addTodoHandler = (title) => {
        const newTodo = {
            title: title
        };
        console.log("taqui o bicho" + title);
        this.setState(prevState => ({
            todos: [...prevState.todos, newTodo]
          }));
    }

    removeTodoHandler = (id) => {

    }

    render() {
        return (
            <div className="todo-app-container">
                <div className="todo-app-container__title">
                    <h1 className="main-title">Task Manager</h1>
                </div>
                <TodoInput created={this.addTodoHandler}/>
                <TodoList todos={this.state.todos}/>
            </div>
        )
    }
}

export default TodoApp;