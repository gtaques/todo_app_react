import React, {Component} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const TodoSingle = (props) => {
    return(
        <div className="todo-single">
            <div className="todo-single__text-display">
                <label className="label-todo-name">{props.title}</label>
            </div>
            <div className="todo-single__buttons">
                <button className="button-action btn-edit"><FontAwesomeIcon icon="edit" /></button>
            </div>
            <div className="todo-single__buttons">
                <button className="button-action btn-delete"><FontAwesomeIcon icon="trash-alt" /></button>
            </div>
            
        </div>
    )
}

export default TodoSingle;