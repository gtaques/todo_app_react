import React, {Component} from 'react';

class TodoInput extends Component {
    state = {
        title: ""
    }
    

    handleCreateTodo = (e) => {
        e.preventDefault();
        this.props.created(this.state.title);
        console.log(this.state.title);
    }

    render(){
        return(
            <div className="todo-create-form">
                <div className="todo-create-form__input">
                    <input className="todo-create-input" onChange={(event) => this.setState({title: event.target.value})} type="text"/>
                </div>
                <div className="todo-create-form__button">
                    <button className="todo-create-button" onClick={() => this.props.created(this.state.title)}>Add</button>
                </div>
            </div>
        )
    }
}

export default TodoInput;