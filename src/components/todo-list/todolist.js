import React, { Component } from 'react';
import TodoSingle from '../todo-single/todosingle';

const TodoList = (props) => {  
    const todos = props.todos.map((todo,index) => {
        return(
            <TodoSingle key={todo.title + index} title={todo.title}/>
        )
    })

    return (
        <div className="todo-list">
            <ul className="todo-list__todos">
                {todos}
            </ul>
        </div>
    )
}



export default TodoList;